import { Component, Inject, Input } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

export interface CategoryCreationConfirmation {
  onConfirm
}

@Component({
  selector: "app-category-creation-dialog",
  templateUrl: "category-creation.dialog.html",
})
export class CategoryCreationDialog {
  private catName: string;

  constructor(
    public dialogRef: MatDialogRef<CategoryCreationDialog>,
    @Inject(MAT_DIALOG_DATA) public data: CategoryCreationConfirmation
  ) {
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  confirmAction() {
    this.dialogRef.close(true);
    this.data.onConfirm( this.catName );
  }
}
