import { Component, Inject, Input, OnDestroy, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FacadeService } from 'src/app/_services/facade.service';
import { Category } from 'src/app/_entities/category.entity';
import { Subscription } from 'rxjs';

export interface TaskCreationConfirmation {
  onConfirm;
}

@Component({
  selector: "app-task-creation-dialog",
  templateUrl: "task-creation.dialog.html",
})
export class TaskCreationDialog implements OnInit, OnDestroy {
  private pickedCategory : Category;
  private taskName : string;
  private categories : Category[];

  private updateSubscription : Subscription;

  constructor(
    private facadeService : FacadeService,
    public dialogRef: MatDialogRef<TaskCreationDialog>,
    @Inject(MAT_DIALOG_DATA) public data: TaskCreationConfirmation
  ) {
    
  }

  ngOnInit() {
    this.categories = this.facadeService.getAllCategory();
    this.updateSubscription = this.facadeService.subscribeToUpdate( () => {
      this.categories = this.facadeService.getAllCategory();
    } )
  }

  ngOnDestroy() {
    this.updateSubscription.unsubscribe();
  }
  

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  confirmAction() {
    this.data.onConfirm( this.pickedCategory, this.taskName );
    this.dialogRef.close(true);
  }
}
