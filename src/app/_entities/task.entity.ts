import {Category} from "./category.entity"
import {Sequence} from "./sequence.entity"

export class Task{


    public id : number ;

    public name : string;

    public category : number;

    public creation : Date;

    public sequences : Array< Sequence >;
    
    
}