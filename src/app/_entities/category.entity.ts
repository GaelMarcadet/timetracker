import {Task} from "./task.entity"

export class Category
{
    public id : number;

    public name : string;

    public tasks : Array<Task>;
}