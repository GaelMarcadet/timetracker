import { Category } from './category.entity';

export class User {
	public username : string;
	public  password : string;
	public singleTasks : Category;
	public categories : Array<Category>;
}