import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveTasksViewComponent } from './active-tasks-view.component';

describe('ActiveTasksViewComponent', () => {
  let component: ActiveTasksViewComponent;
  let fixture: ComponentFixture<ActiveTasksViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveTasksViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveTasksViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
