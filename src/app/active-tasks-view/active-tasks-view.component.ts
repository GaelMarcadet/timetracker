import { Component, OnInit } from '@angular/core';
import { Task } from '../_entities/task.entity';
import { FacadeService } from '../_services/facade.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-active-tasks-view',
  templateUrl: './active-tasks-view.component.html',
  styleUrls: ['./active-tasks-view.component.css']
})
export class ActiveTasksViewComponent implements OnInit {

  private activeTasks : Array< Task >; 
  private displayedColumns : string[] = [ "#", "Name", "Started At" ]

  constructor( private facade : FacadeService, private router : Router ) { 
      
  }

  ngOnInit() {
    this.activeTasks = this.facade.getActiveTasks();
  }

  goToTask( task : Task ) {
    this.router.navigate([ "/tasktimer/task/" + task.id ]);
  } 

}
