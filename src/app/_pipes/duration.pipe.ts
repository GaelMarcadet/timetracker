import { Pipe, PipeTransform } from '@angular/core';
import { Sequence } from '../_entities/sequence.entity';
import { DurationComputer } from '../_services/duration.service';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {



  transform( sequence : Sequence ): any {
      console.log( "DurationPipe: params: ", sequence )
      const start = sequence.start;
      const end = sequence.end ? sequence.end : new Date();
      return DurationComputer.diff( start, end );
  }
}
