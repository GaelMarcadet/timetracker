import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Category } from '../_entities/category.entity';
import { Task } from '../_entities/task.entity';
import { Router } from '@angular/router';
import { FacadeService } from '../_services/facade.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  // categories table columns
  private categoriesTableColumns = [ "name", "nb-tasks", "action" ]
  private categoriesDataSource : MatTableDataSource< Category > = new MatTableDataSource();

  private tasks : Task[];

 

  constructor( private facadeService : FacadeService, private router : Router ) {
    
  }

  ngOnInit() {
    
    this.facadeService.subscribeToUpdate( _ => this.paintComponentWithData() );
    this.paintComponentWithData();
  }

  //////////////////////////////////////////////////////////////////////////////////
  // CATEGORIES ACTIONS
  //////////////////////////////////////////////////////////////////////////////////
  applyCategoriesFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.categoriesDataSource.filter = filterValue.trim().toLowerCase();
  }

  goToCategory( category : Category ) {
    return this.router.navigate([ "/tasktimer/category/" + category.id ])
  }
  


  paintComponentWithData() {
    this.categoriesDataSource.data = this.facadeService.getAllCategory();
    
    this.tasks = this.facadeService.getAllTask();
  }


/*

  private currentTasks : Task[];

  private categories : Category[];

  private currentCategory : Category;

  private totalTimeSpend : number = 0;

  private show : boolean = false;

  private wantAddCategory : boolean = false;

  private wantAddTask : boolean = false;

  private name : string = "";


  constructor(private router : Router, private facadeService : FacadeService) { }

  ngOnInit() {
    this.categories = this.facadeService.getAllCategory();
    this.currentTasks = null;
  
  }

  categoryChoosen(category : Category){
    this.currentCategory = category;
    this.currentTasks = this.facadeService.getAllTaskOfCategory(category.id);
    this.show = false;
    this.wantAddCategory = false;
    this.wantAddTask = false;
  }

  taskChoosen(task : Task){
    
    this.router.navigate([ "tasktimer/task/"+task.id ])
  }

  removeCategory(){
    this.facadeService.removeCategory(this.currentCategory);
    this.currentCategory = null;
  }

  all(){
    this.currentTasks = this.facadeService.getAllTask();
    this.currentCategory = null;
    this.show = true;
  }

  addCategory(){
    this.wantAddCategory = true;
  }

  addCategoryValid(){
    if(this.name != ""){
      this.facadeService.addCategory(this.name);
    }
    this.wantAddCategory = false;
  }

  addTask(){
    this.wantAddTask = true;
  }

  addTaskValid(){
    if(this.name != ""){
      this.facadeService.addTask(this.currentCategory.id,this.name);
    }
    this.wantAddTask = false;
    this.currentTasks = this.facadeService.getAllTaskOfCategory(this.currentCategory.id);
  }

  */
}
