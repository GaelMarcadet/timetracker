import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import { AuthenticationService } from './_services/authentiction.services';
import { DataHolder } from './_services/data_holder';
import { AuthenticationGuard } from './_services/authentication.guards';
import { FacadeService } from './_services/facade.service';


import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { FormsModule } from  '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

// Material
import { MatButtonModule, MatButtonToggleGroup, MatDialogModule, MatTabsModule, MatPaginatorModule, MatSlideToggleModule, MatBadgeModule, MatCheckboxModule, MatSelectModule } from "@angular/material";
import { MatFormFieldModule } from '@angular/material';
import { CategoryComponent, ConfirmationDialog } from './category/category.component';
import { TaskComponent } from './task/task.component';
import { MatInputModule, MatGridListModule, MatToolbarModule, MatMenuModule, MatIconModule, MatTableModule, MatButtonToggleModule, MatSnackBarModule } from '@angular/material';
import { NotFoundComponent } from './errors/not-found/not-found.component';
import {MatSidenavModule} from '@angular/material';
import {MatListModule} from '@angular/material/list';
import { ActiveTasksViewComponent } from './active-tasks-view/active-tasks-view.component';
import { TaskTimerComponent } from './task-timer/task-timer.component';
import { DurationPipe } from './_pipes/duration.pipe';
import { TasksTableViewComponent } from './tasks-table-view/tasks-table-view.component';
import { TaskCreationDialog } from './dialogs/task-creation-dialog/task-creation.dialog';
import { CategoryCreationDialog } from './dialogs/category-creation-dialog/category-creation.dialog';


@NgModule({
  declarations: [
    AppComponent,
    AuthenticationComponent,
    HomeComponent,
    CategoryComponent,
    TaskComponent,
    NotFoundComponent,
    ActiveTasksViewComponent,
    TaskTimerComponent,
    DurationPipe,
    ConfirmationDialog,
    TasksTableViewComponent,
    TaskCreationDialog,
    CategoryCreationDialog,
  ],
  entryComponents: [
    ConfirmationDialog,
    TaskCreationDialog,
    CategoryCreationDialog,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatGridListModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatTableModule,
    MatButtonToggleModule,
    MatSnackBarModule,
    MatDialogModule,
    MatTabsModule,
    MatPaginatorModule,
    MatSlideToggleModule,
    MatBadgeModule,
    MatCheckboxModule,
    MatSelectModule,
  ],
  providers: [
    AuthenticationGuard,
    AuthenticationService,
    DataHolder,
    FacadeService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
