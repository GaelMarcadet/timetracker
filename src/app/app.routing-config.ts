export enum RoutingConfig {
  NOT_FOUND = "not-found",
  HOME = "/tasktimer/home",
  LOGIN = "/login",
}