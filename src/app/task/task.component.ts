import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FacadeService } from "../_services/facade.service";
import { Task } from "../_entities/task.entity";
import { Sequence } from "../_entities/sequence.entity";
import { MatTableDataSource, MatSnackBar } from "@angular/material";
import { interval } from "rxjs";
import { DurationComputer } from "../_services/duration.service";
import { RoutingConfig } from '../app.routing-config';

@Component({
  selector: "app-task",
  templateUrl: "./task.component.html",
  styleUrls: ["./task.component.css"],
})
export class TaskComponent implements OnInit {
  private task: Task;
  private sequencesDataSource: MatTableDataSource<Sequence>;

  private displayedColumns = ["start", "end", "duration", "action"];
  private durations: Map<Sequence, string>;
  private taskCounter : string;

  private sequenceStart : Date;
  private sequenceEnd? : Date;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private facadeService: FacadeService,
    private _snackBar: MatSnackBar
  ) {
    const taskId = +activatedRoute.snapshot.params["id"];
    if (!facadeService.taskExist(taskId)) {
      router.navigate([ RoutingConfig.NOT_FOUND ]);
    }

    this.task = facadeService.getTaskById(taskId);
    this.sequencesDataSource = new MatTableDataSource(this.task.sequences);

    this.durations = new Map();
    this.updateDurations();
    this.updateTotalDuration();
    let counter = interval(1000);
    counter.subscribe(() => {
        this.updateDurations();
        this.updateTotalDuration();
    });
  }

  ngOnInit() {}

  createSequence() {
    let start = new Date();//new Date( this.sequenceStart );
    let end = undefined;
    if ( this.sequenceEnd ) {
      end = new Date( this.sequenceEnd );
    } else { 
      if ( this.isActiveTask() ) {
        this.stopTask();
      }
    }
    const seq = new Sequence();
    seq.start = start;
    seq.end = end;
    this.task.sequences.push( seq );
    this.sequencesDataSource.data = this.task.sequences;

    this.sequenceStart = undefined;
    this.sequenceEnd = undefined;
  }

  isActiveTask() {
    return this.facadeService.isActiveTask(this.task);
  }

  startTask() {
    this.facadeService.startTask(this.task);
    this.sequencesDataSource.data = this.task.sequences;
  }

  stopTask() {
    this.facadeService.stopTask(this.task);
    this.sequencesDataSource.data = this.task.sequences;
  }

  deleteSequence(sequence) {
    this.facadeService.deleteSequence(this.task, sequence);
    this.sequencesDataSource.data = this.task.sequences;
  }

  isActiveSequence(sequence: Sequence) {
    if (sequence.end) {
      return false;
    } else {
      return true;
    }
  }

  stopSequence(sequence: Sequence) {
    sequence.end = new Date();
    this._snackBar.open("The sequence has been stopped", "Close", {
      duration: 2000,
    });
  }

  updateDurations() {
    for (let index = 0; index < this.task.sequences.length; index++) {
      const start = this.task.sequences[index].start;
      const end = this.task.sequences[index].end
        ? this.task.sequences[index].end
        : new Date();
      this.durations[index] = DurationComputer.diff(start, end);
    }
  }

   updateTotalDuration() {
    let totalDuration = 0;
    for (let index = 0; index < this.task.sequences.length; index++) {
      const start = this.task.sequences[index].start;
      const end = this.task.sequences[index].end
        ? this.task.sequences[index].end
        : new Date();
      totalDuration += end.getTime() - start.getTime();
    }
    this.taskCounter = DurationComputer.millisecondsToDuration( totalDuration );
  }
}
