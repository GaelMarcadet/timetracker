import { Component, OnInit, OnChanges, ChangeDetectorRef, OnDestroy, Inject, NgModule } from "@angular/core";
import { Category } from "../_entities/category.entity";
import { Task } from "../_entities/task.entity";
import { Router, ActivatedRoute, RouteConfigLoadEnd, RouteReuseStrategy, RouterEvent, NavigationEnd } from "@angular/router";
import { FacadeService } from "../_services/facade.service";
import { RoutingConfig } from "../app.routing-config";
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { interval, Subscription } from 'rxjs';
import { DurationComputer } from '../_services/duration.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: "app-category",
  templateUrl: "./category.component.html",
  styleUrls: ["./category.component.css"],
})
export class CategoryComponent implements OnInit, OnDestroy{
  private category : Category;


  // task creation
  displayTaskCreationForm : boolean = false;
  taskName : string;

  // category edition
  displayCategoryEditionForm : boolean = false;

  // category deletion
  deleteCategoryConfirmation : boolean;

  
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private facadeService: FacadeService,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {

  }



  ngOnInit() {
    this.activatedRoute.params.subscribe(
      routeParams => {
        const categoryId = +routeParams.id;
        // if no one category matches with specified id, redirect to not found
        if (!this.facadeService.categoryExists(categoryId)) {
          this.router.navigate([ RoutingConfig.NOT_FOUND ]);
        }
  
        this.refreshComponentWithCategory( this.facadeService.getCategoryByID( categoryId ) ); 
        
      }
    )
  }


  ngOnDestroy() {

  }

  deleteCategory() {
    const dialogRef = this.dialog.open(ConfirmationDialog, {
      width: '300px',
      height: '200px',
      data: {
        confirm: this.deleteCategoryConfirmation,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      // delete category
      this.facadeService.deleteCategory( this.category );
      this.router.navigate([ "/" + RoutingConfig.HOME ])
    });
  }

  //////////////////////////////////////////////////////////////////////////
  // TASK CREATION
  /////////////////////////////////////////////////////////////////////////
  displayTaskCreation() {
    this.displayTaskCreationForm = !this.displayTaskCreationForm;
    this.displayCategoryEditionForm = false;
  }

  createTask() {
    if ( this.taskName ) {
      // create task
      const createdTask : Task = this.facadeService.createTask( this.category, this.taskName );
      
      
      // update view to notify task successfully created
      this._snackBar.open( "Task \"" + this.taskName + "\" created", "OK", {
        duration: 2000,
      });    
      this.taskName = "";
      this.displayTaskCreationForm = false;
    } 
  }

  //////////////////////////////////////////////////////////////////////////
  // CATEGORY EDITION
  /////////////////////////////////////////////////////////////////////////
  displayCategoryEdition() {
    this.displayCategoryEditionForm = !this.displayCategoryEditionForm;
    this.displayTaskCreationForm = false;
  }




  

  refreshComponentWithCategory( category : Category ) : void {
    this.category = category;
  } 

  
}


export interface Confirmation {
  confirm : boolean;
}


@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialogs/confirmation-dialog.html',
})
export class ConfirmationDialog {

  constructor(
    public dialogRef: MatDialogRef<ConfirmationDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Confirmation) {}

  onNoClick(): void {
    this.dialogRef.close( false );
  }

  confirmDelete() {
    this.dialogRef.close( true );
  }
  
}