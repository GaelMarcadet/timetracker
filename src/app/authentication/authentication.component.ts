import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services/authentiction.services';
import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { FacadeService } from '../_services/facade.service';
import { RoutingConfig } from '../app.routing-config';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

  private static nextUrlQueryParamName : string = "next_url";
  private username : string;
  private password : string;
  private nextUrl : string;
  private authenticationFailed : boolean = false;

  constructor( private facadeService : FacadeService, private router : Router, private route : ActivatedRoute ) { }

  ngOnInit() {
    if ( this.route ) {
      this.nextUrl = this.route.snapshot.queryParamMap.get( AuthenticationComponent.nextUrlQueryParamName );
    }
  }

  login() {
    this.facadeService.login( this.username, this.password )
    .then( () => {
      if ( this.nextUrl ) {
        if ( this.nextUrl.startsWith( "/" ) ) {
          this.router.navigate([ this.nextUrl ])
        } else {
          this.router.navigateByUrl( this.nextUrl );
        }
        
      } else {
        this.router.navigate([ "/" + RoutingConfig.HOME ])
      }
       
    })
    .catch( 
      () => {
        this.password = "";
        this.authenticationFailed = true;
        console.log( "Login failed" );
      }
    )
  }

}
