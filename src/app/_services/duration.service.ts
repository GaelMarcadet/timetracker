export class DurationComputer {
  public static diff(start: Date, end: Date): string {
    return DurationComputer.millisecondsToDuration( end.getTime() - start.getTime() )
  }

  public static millisecondsToDuration(counter): string {
    let diff_seconds: number = Math.round(counter / 1000);
    let diff_minutes = diff_seconds / 60;
    diff_seconds = diff_seconds % 60;
    let diff_hours = diff_minutes / 60;
    diff_minutes = diff_minutes % 60;
    let diff_days = diff_hours / 24;
    diff_hours = diff_hours % 24;

    diff_days = Math.round(diff_days);
    diff_hours = Math.round(diff_hours);
    diff_minutes = Math.round(diff_minutes);
    diff_seconds = Math.round(diff_seconds);

    return (
      (diff_days != 0 ? diff_days + "d " : "") +
      (diff_hours != 0 ? diff_hours + "h " : "") +
      (diff_minutes != 0 ? diff_minutes + "m " : "") +
      diff_seconds +
      "s"
    );
  }
}
