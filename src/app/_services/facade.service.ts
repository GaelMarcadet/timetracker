import { Facade } from "./facade";
import { Category } from "../_entities/category.entity";
import { Task } from "../_entities/task.entity";
import { DataHolder } from "./data_holder";
import { Injectable } from "@angular/core";
import { User } from "../_entities/user.entity";
import { AuthenticationService } from "./authentiction.services";
import { Sequence } from "../_entities/sequence.entity";
import { Observable } from "rxjs/internal/Observable";
import { Subject, Subscription } from "rxjs";

@Injectable()
export class FacadeService implements Facade {
  private static reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/;
  private static reMsAjax = /^\/Date\((d|-|.*)\)[\/|\\]$/;

  private dateParser = function (key, value) {
    if (typeof value === "string") {
      // try to parse as ISO format
      var formatType = FacadeService.reISO.exec(value);
      if (formatType) return new Date(value);

      // try to parse as MsAjax format
      formatType = FacadeService.reMsAjax.exec(value);
      if (formatType) {
        var b = formatType[1].split(/[-+,.]/);
        return new Date(b[0] ? +b[0] : 0 - +b[1]);
      }
    }
    return value;
  };

  private static ID = 100;

  private singleTasks: Category;

  private categories: Category[];

  private tasks: Task[];

  private activeTasks: Task[];

  private authenticatedUser: User;

  private updateObservable: Subject<User>;

  public constructor(
    private authService: AuthenticationService,
    private dataHolder: DataHolder
  ) {
    this.updateObservable = new Subject();
    if (authService.isAuthenticated()) {
      this.customizeDataFromUser(authService.authenticatedUser());
    }
  }

  subscribeToUpdate(observer): Subscription {
    return this.updateObservable.subscribe(observer);
  }

  createTask(category: Category, taskName: string) {
    const createdTask = new Task();
    createdTask.id = this.getIdAndIncrement();
    createdTask.sequences = new Array();
    createdTask.category = category.id;
    createdTask.creation = new Date();
    createdTask.name = taskName;

    category.tasks.push(createdTask);

    this.notifyModification();
    return createdTask;
  }

  createQuickTask() {
    // initialize the new task as anynymous
    const task: Task = new Task();
    task.id = FacadeService.ID;
    task.name = "Quick Task " + FacadeService.ID;
    FacadeService.ID++;
    task.creation = new Date();
    task.sequences = new Array();
    this.singleTasks.tasks.push(task);

    // a quick task is started by default
    this.startTask(task);

    // Norify the oberservers that a new task has been created
    this.notifyModification();
  }

  createCategory(categoryName: any) {
    const category: Category = new Category();
    category.id = this.getIdAndIncrement();
    category.name = categoryName;
    category.tasks = new Array<Task>();
    this.authenticatedUser.categories.push(category);

    this.notifyModification();
  }

  deleteCategory(category: Category) {
    for (
      let index = 0;
      index < this.authenticatedUser.categories.length;
      index++
    ) {
      const catIndex = this.authenticatedUser.categories[index].id;
      if (catIndex === category.id) {
        console.log("Sequence found and deleted");
        this.authenticatedUser.categories.splice(index, 1);
        break;
      }
    }
    this.notifyModification();
  }

  getSingleTasks(): Category {
    return this.singleTasks;
  }
  categoryExists(categoryIndex: number): boolean {
    if (this.getCategoryByID(categoryIndex)) {
      return true;
    } else {
      return false;
    }
  }
  deleteSequence(task: Task, sequence: Sequence) {
    console.log(
      "Deleting sequence ",
      sequence,
      " from task ",
      task.sequences.length
    );
    for (let index = 0; index < task.sequences.length; index++) {
      const sec = task.sequences[index];
      if (sec.start == sequence.start && sec.end == sequence.end) {
        console.log("Sequence found and deleted");
        task.sequences.splice(index, 1);
        break;
      }
    }
    console.log(" ", sequence, " from task ", task.sequences.length);
  }

  stopTask(task: Task) {
    for (const seq of task.sequences) {
      if (!seq.end) {
        seq.end = new Date();
      }
    }
    this.notifyModification();
  }

  startTask(task: Task) {
    const seq = new Sequence();
    seq.start = new Date();
    task.sequences.push(seq);
    console.log("Starting task:", task);
    this.notifyModification();
  }

  taskExists(taskId: number) {
    if (this.getTaskById(taskId)) {
      return true;
    } else {
      return false;
    }
  }

  insertSequenceInTask(task: Task, sequence: Sequence) {
    task.sequences.push(sequence);
    task.sequences.sort((seq1: Sequence, seq2: Sequence) => {
      if (seq1.end && seq2.end) {
        return seq1.end < seq2.end ? -1 : 1;
      } else {
        return seq1.start < seq2.start ? -1 : 1;
      }
    });
  }

  login(username: string, password: string) {
    return new Promise((resolve, reject) => {
      this.authService
        .login(username, password)
        .then((user: User) => {
          this.customizeDataFromUser(user);
          resolve(user);
        })
        .catch(() => reject());
    });
  }

  getAllCategory(): Category[] {
    return this.authenticatedUser.categories;
  }

  getAllTask(): Task[] {
    const tasks = new Array();
    // push tasks from single tasks
    for (const task of this.authenticatedUser.singleTasks.tasks) {
      tasks.push(task);
    }

    // push tasks from categories
    for (let category of this.authenticatedUser.categories) {
      for (let task of category.tasks) {
        tasks.push(task);
      }
    }

    return tasks;
  }

  getAllTaskOfCategory(idCategory: number): Task[] {
    return this.tasks.filter((task) => task.category == idCategory);
  }

  taskExist(taskId: number): boolean {
    return this.taskExists(taskId);
  }

  getTaskById(taskId: number): Task {
    return this.tasks.find((task) => task.id == taskId);
  }

  getCategoryByID(categoryId: number): Category {
    if (this.singleTasks.id === categoryId) {
      return this.singleTasks;
    }
    return this.categories.find((category) => category.id === categoryId);
  }

  removeCategory(currentCategory: Category) {
    this.categories.forEach((item, index) => {
      if (item.id === currentCategory.id) this.categories.splice(index, 1);
    });

    this.tasks.forEach((item, index) => {
      if (item.category === currentCategory.id) this.tasks.splice(index, 1);
    });
  }

  addCategory(name: string) {
    this.categories.push({ id: FacadeService.ID, name: name, tasks: [] });
    FacadeService.ID++;
  }

  addTask(categoryId: number, name: string) {
    const task: Task = {
      id: FacadeService.ID,
      name: name,
      category: categoryId,
      creation: new Date(),
    } as Task;
    FacadeService.ID++;

    let category = this.getCategoryByID(categoryId);
    category.tasks.push(task);
    this.tasks.push(task);
  }

  getActiveTasks(): Array<Task> {
    return this.getAllTask().filter((task, index) => this.isActiveTask(task));
  }

  isActiveTask(task: Task): boolean {
    // try to speed up process by order sequence
    for (let sequence of task.sequences) {
      if (!sequence.end) {
        return true;
      }
    }
    return false;
  }

  getTotalDuration(task: Task): number {
    let totalDuration = 0;
    for (let index = 0; index < task.sequences.length; index++) {
      const start = task.sequences[index].start;
      const end = task.sequences[index].end
        ? task.sequences[index].end
        : new Date();
      totalDuration += end.getTime() - start.getTime();
    }
    return totalDuration;
  }

  private customizeDataFromUser(user: User) {
    const data = this.dataHolder
      .getData()
      .find((u) => user.username == u.username);
    this.authenticatedUser = data;
    this.categories = data.categories;
    this.singleTasks = data.singleTasks;

    // retreive all tasks
    this.tasks = new Array();
    for (let category of this.categories) {
      for (let task of category.tasks) {
        this.tasks.push(task);
      }
    }

    // retreive active tasks
    this.activeTasks = new Array();
    for (const task of this.tasks) {
      if (this.isActiveTask(task)) {
        this.activeTasks.push(task);
      }
    }
  }

  private notifyModification() {
    this.updateObservable.next(this.authenticatedUser);
  }

  private getIdAndIncrement(): number {
    const providedId = FacadeService.ID;
    FacadeService.ID++;
    return providedId;
  }
}
