import { Category } from "../_entities/category.entity"
import { Task } from "../_entities/task.entity"


export interface Facade{

    getAllCategory() : Category[];

    getAllTask() : Task[];

    getAllTaskOfCategory( idCategory : number ) : Task[];

    taskExist(taskId : number) : boolean;

    getTaskById(taskId: number): Task;

    removeCategory(currentCategory: Category);
}