import { User } from '../_entities/user.entity';
import { Category } from '../_entities/category.entity';
import { Task } from '../_entities/task.entity';

export class DataHolder {
	public data : User[] = [
		{
			username: "gael",
			password: "gael",
			singleTasks: {
				id: 0,
				name: "SingleTasks",
				tasks: [
					{
						id: 0,
						name: "Start",
						creation: new Date(),
						category: 0,
						sequences: [],
					}
				]
			}, 
			categories: [
				{
					id: 1,
					name: "Category 1",
					tasks: [
						{
							id: 11,
							name: "Task 1 1",
							creation: new Date(),
							category: 1,
							sequences: [
								{
									start: new Date( 2019, 1 ),
									end: new Date( 2019, 2 ),
								}
							],
						},
						{
							id: 12,
							name: "Task 1 2",
							creation: new Date(),
							category: 1,
							sequences: [
								{
									start: new Date( 2019, 2, 25, 15, 20 ),
									end: new Date( 2019, 2, 25, 15, 36 ),
								},
								{
									start : new Date( 2020, 3 ),
									end: null,
								},
							],
						},
					]
				},
				{
					id: 2,
					name: "Category 2",
					tasks: [
						{
							id: 21,
							name: "Task 2 1",
							creation: new Date(),
							category: 2,
							sequences: [],
						},
						{
							id: 22,
							name: "Task 2 2",
							creation: new Date(),
							category: 1,
							sequences: [],
						},
					]
				},
				{
					id: 3,
					name: "Category 3",
					tasks: []
				},
				{
					id: 4,
					name: "Category 4",
					tasks: []
				},
				{
					id: 5,
					name: "Category 5",
					tasks: []
				},
				{
					id: 6,
					name: "Category 6",
					tasks: []
				},
				{
					id: 7,
					name: "Category 3",
					tasks: []
				},
				{
					id: 8,
					name: "Category 8",
					tasks: []
				},
				{
					id: 9,
					name: "Category 9",
					tasks: []
				},
				{
					id: 10,
					name: "Category 10",
					tasks: []
				},
			]
		}];
		
	public getData() : User[]{
		return this.data;
	}

}