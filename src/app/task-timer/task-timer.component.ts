import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Task } from '../_entities/task.entity';
import { Category } from '../_entities/category.entity';
import { FacadeService } from '../_services/facade.service';

@Component({
  selector: 'app-task-timer',
  templateUrl: './task-timer.component.html',
  styleUrls: ['./task-timer.component.css']
})
export class TaskTimerComponent implements OnInit {

  private currentTasks : Task[];

  private categories : Category[];

  private currentCategory : Category;

  private totalTimeSpend : number = 0;

  private show : boolean = false;

  private wantAddCategory : boolean = false;

  private wantAddTask : boolean = false;

  private name : string = "";



  events: string[] = [];
  opened: boolean;

  shouldRun = [/(^|\.)plnkr\.co$/, /(^|\.)stackblitz\.io$/].some(h => h.test(window.location.host));


  constructor(private router : Router, private facadeService : FacadeService) { }

  ngOnInit() {

    this.categories = this.facadeService.getAllCategory();
    this.currentTasks = null;

  }

  categoryChoosen(category : Category){
    console.log( "Choosing category ", category )
    this.router.navigateByUrl("/tasktimer/category/" + category.id )
  }

  createQuickTask() {
    const singleTasks : Category = this.facadeService.getSingleTasks();
    this.facadeService.createQuickTask();
    this.router.navigate([ "/tasktimer/category/" + singleTasks.id ])
  }

  taskChoosen(task : Task){
    
    this.router.navigate([ "task/"+task.id ])
  }

  removeCategory(){
    this.facadeService.removeCategory(this.currentCategory);
    this.currentCategory = null;
  }

  goToGeneralCategory(){
    const singleTasks : Category = this.facadeService.getSingleTasks();
    console.log( singleTasks )
    return this.router.navigate([ "/tasktimer/category/" + singleTasks.id ])
  }

  addCategory(){
    this.wantAddCategory = true;
  }

  addCategoryValid(){
    if(this.name != ""){
      this.facadeService.addCategory(this.name);
    }
    this.wantAddCategory = false;
  }

  addTask(){
    this.wantAddTask = true;
  }

  addTaskValid(){
    if(this.name != ""){
      this.facadeService.addTask(this.currentCategory.id,this.name);
    }
    this.wantAddTask = false;
    this.currentTasks = this.facadeService.getAllTaskOfCategory(this.currentCategory.id);
  }

}
