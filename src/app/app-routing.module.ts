import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticationGuard } from './_services/authentication.guards';
import { AuthenticationComponent } from './authentication/authentication.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './errors/not-found/not-found.component';
import { CategoryComponent } from './category/category.component';
import { TaskComponent } from './task/task.component';
import { TaskTimerComponent } from './task-timer/task-timer.component';
import { RoutingConfig } from './app.routing-config';



const guards = [
  AuthenticationGuard
]

const routes: Routes = [
  // login page, no guards for this route
  { path: "login", component: AuthenticationComponent }, 
  { path: "", pathMatch: "full", redirectTo: RoutingConfig.HOME },

  // default route redirected to home component
  { 
      path: "tasktimer", 
      component: TaskTimerComponent,
      children: [
        { path: "home", component: HomeComponent, canActivate: guards },
        { path: "category/:id", component: CategoryComponent, canActivate: guards },
        { path: "task/:id", component : TaskComponent, canActivate : guards },
      ] 
    },
  
  
  
  ///////////////////////////////////////////////////////////////
  // Error pages
  ///////////////////////////////////////////////////////////////
  // 404 not found component
  { path: "**", canActivate: guards, redirectTo: 'not-found' },
  { path: 'not-found', canActivate: guards, component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
