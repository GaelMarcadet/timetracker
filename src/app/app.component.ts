import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "./_services/authentiction.services";
import { Router } from "@angular/router";
import { RoutingConfig } from "./app.routing-config";
import { FacadeService } from "./_services/facade.service";
import { MatDialog, MatSnackBar } from "@angular/material";
import { CategoryCreationDialog } from "./dialogs/category-creation-dialog/category-creation.dialog";
import { TaskCreationDialog } from "./dialogs/task-creation-dialog/task-creation.dialog";
import { Category } from "./_entities/category.entity";

export class Test {
  now: Date;
}

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  title = "TaskTimer";
  nbActiveTasks: number = 0;

  constructor(
    private _snack: MatSnackBar,
    private authService: AuthenticationService,
    private router: Router,
    private facadeService: FacadeService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.paintComponentWithData();
    this.facadeService.subscribeToUpdate((_) => this.paintComponentWithData());
  }

  logout() {
    this.authService.logout();
    this.router.navigate([RoutingConfig.LOGIN]);
  }

  paintComponentWithData() {
    this.nbActiveTasks = this.facadeService.getActiveTasks().length;
  }

  goToHome() {
    this.router.navigate([RoutingConfig.HOME]);
  }

  createQuickTask() {
	this.facadeService.createQuickTask();
	this._snack.open('Quick Task created and started', "OK", {
		duration: 2000,
	  });
  }

  displayCategoryCreation() {
    this.dialog.open(CategoryCreationDialog, {
      width: "500px",
      height: "400px",
      data: {
        onConfirm: (categoryName) => {
          // if category creation is confirmed, creates her and notify user
          this.facadeService.createCategory(categoryName);
          this._snack.open('Category "' + categoryName + '" created', "OK", {
            duration: 2000,
          });
        },
      },
    });
  }

  displayTaskCreation() {
    this.dialog.open(TaskCreationDialog, {
      width: "500px",
      height: "400px",
      data: {
        onConfirm: (category: Category, taskName: string) => {
          // if task creation is confirmed, creates it, notifies user of the creation and go one the new task
          const createdTask = this.facadeService.createTask(category, taskName);
          this._snack.open(
            'Task "' +
              taskName +
              '" created under Category "' +
              category.name +
              '"',
            "OK",
            {
              duration: 2000,
            }
          );
          this.router.navigate(["/tasktimer/task/" + createdTask.id]);
        },
      },
    });
  }
}
